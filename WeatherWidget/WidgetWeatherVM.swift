//
//  File.swift
//  WeatherWidgetExtension
//
//  Created by Marek Štovíček on 24/10/2020.
//

import Foundation
import SwiftUI
import Combine

class WidgetWeatherVM: ObservableObject {
    
    // MARK: - Variables
    // published
    @Published var weather: CityWeather?
    
    // private
    private let locationManager: LocationManager
    private var timer: Timer?
    private var cancelable = Set<AnyCancellable>()
    
    // MARK: - Initializer
    init(locationManager: LocationManager) {
        self.locationManager = locationManager
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.startDataUpdate()
        }
    }
    
    // MARK: - Content
    private func startDataUpdate() {
        timer = Timer.scheduledTimer(timeInterval: 10,
                                     target: self,
                                     selector: #selector(loadData),
                                     userInfo: nil, repeats: true)
        timer?.fire()
    }
    
    @objc private func loadData() {
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=\(locationManager.coordinates.latitude)&lon=\(locationManager.coordinates.longitude)&appid=e72a7ca9eea7964968cbfc5661d3a890") else {
            return
        }
        URLSession.shared.dataTaskPublisher(for: url)
            .map(\.data)
            .decode(type: CityWeather.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink { (completion) in
                print(completion)
            } receiveValue: { weather in
                self.weather = weather
            }.store(in: &cancelable)

    }
}
