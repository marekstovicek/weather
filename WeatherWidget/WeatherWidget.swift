//
//  WeatherWidget.swift
//  WeatherWidget
//
//  Created by Marek Štovíček on 17/10/2020.
//

import WidgetKit
import SwiftUI
import Combine

struct Provider: TimelineProvider {
    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date())
    }

    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(date: Date())
        completion(entry)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []

        // Generate a timeline consisting of five entries an hour apart, starting from the current date.
        let currentDate = Date()
        for hourOffset in 0 ..< 5 {
            let entryDate = Calendar.current.date(byAdding: .hour, value: hourOffset, to: currentDate)!
            let entry = SimpleEntry(date: entryDate)
            entries.append(entry)
        }

        let timeline = Timeline(entries: entries, policy: .atEnd)
        completion(timeline)
    }
}

struct SimpleEntry: TimelineEntry {
    let date: Date
}

struct WeatherWidgetEntryView : View {
    var entry: Provider.Entry
    
    @ObservedObject var vm: WidgetWeatherVM
    
    var body: some View {
        vm.weather.map { content in
            VStack {
                Text(content.name)
                    .font(.headline)
                    .foregroundColor(Color.white)
                VStack {
                    Text("\(String(format: "%.1f °C", (content.main.temp - 273.15)))")
                        .foregroundColor(Color.white)
                        .padding()
                }
            }
            .background(Color.green)
        }
    }
}

struct IconImage: View {
    var image: UIImage
    
    var body: some View {
        Image(uiImage: self.image)
            .resizable()
            .frame(width: 20, height: 20, alignment: .leading)
            .padding()
    }
}


@main
struct WeatherWidget: Widget {
    let kind: String = "WeatherWidget"
    var vm = WidgetWeatherVM(locationManager: LocationManager())
    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            WeatherWidgetEntryView(entry: entry, vm: vm)
        }
        .configurationDisplayName("My Widget")
        .description("This is an example widget.")
    }
}

struct WeatherWidget_Previews: PreviewProvider {
    static var previews: some View {
        WeatherWidgetEntryView(entry: SimpleEntry(date: Date()), vm: WidgetWeatherVM(locationManager: LocationManager()))
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
