//
//  CityWeather.swift
//  Weather
//
//  Created by Marek Štovíček on 15/10/2020.
//

import Foundation

struct CityWeather: Codable {
    let coord: Coordinates
    let weather: [Weather]
    let base: String
    let main: Main
    let visibility: Int
    let wind: Wind
    let clouds: Clouds
    let dt: Double
    let name: String
    let cod: Int
    
    struct Coordinates: Codable {
        let lon: Double
        let lat: Double
    }
    
    struct Weather: Codable {
        let id: Int
        let main: String
        let description: String
        let icon: String
    }

    struct Main: Codable {
        let temp: Double
        let pressure: Int
        let humidity: Int
        let temp_min: Double
        let temp_max: Double
    }
    
    struct Wind: Codable {
        let speed: Float
        let deg: Int
    }
    
    struct Clouds: Codable {
        let all: Int
    }
    
    struct Sys: Codable {
        let type: Int
        let id: Int
        let message: Float
        let country: String
        let sunrise: Double
        let sunset: Double
    }
}
