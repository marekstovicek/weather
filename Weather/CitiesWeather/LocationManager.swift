//
//  LocationManager.swift
//  Weather
//
//  Created by Marek Štovíček on 17/10/2020.
//

import Foundation
import CoreLocation
import Combine

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    // MARK: - Variables
    var coordinates: (latitude: Double, longitude: Double) = (0, 0)
    private let manager = CLLocationManager()
    
    override init() {
        super.init()
        
        self.manager.requestAlwaysAuthorization()
        self.manager.requestWhenInUseAuthorization()
        self.manager.delegate = self
        self.manager.desiredAccuracy = kCLLocationAccuracyBest
        
        self.startCheckLocation()
    }
    
    deinit {
        manager.stopUpdatingLocation()
    }
    // MARK: - Content
    private func startCheckLocation() {
        manager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        self.coordinates = (location.coordinate.latitude.magnitude, location.coordinate.longitude.magnitude)
    }
}
