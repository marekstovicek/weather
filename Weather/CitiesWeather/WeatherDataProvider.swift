//
//  WeatherDataProvider.swift
//  Weather
//
//  Created by Marek Štovíček on 18/10/2020.
//

import Foundation
import Combine

protocol WeatherDataProviderType {
    func loadLocationWeather(latitude: Double, longitude: Double) -> AnyPublisher<CityWeather, NetworkError>
    func loadWeather(for cityName: String) -> AnyPublisher<CityWeather, NetworkError>
}

class WeatherDataProvider: WeatherDataProviderType {
    
    // MARK: - Variables
    private let apiClient: ApiClientType
    
    
    // MARK: - Initializer
    init(apiClient: ApiClientType) {
        self.apiClient = apiClient
    }
    
    // MARK: - Public
    func loadLocationWeather(latitude: Double, longitude: Double) -> AnyPublisher<CityWeather, NetworkError> {
        let router = Router(path: "weather",
                            parameters: [
                                "lat": "\(latitude)",
                                "lon": "\(longitude)"
                            ])
        return apiClient.dataTaskPublisher(for: router, decoder: nil)
            .eraseToAnyPublisher()
    }
    
    func loadWeather(for cityName: String) -> AnyPublisher<CityWeather, NetworkError> {
        let router = Router(path: "weather",
                            parameters: ["q": cityName])
        return apiClient.dataTaskPublisher(for: router, decoder: nil)
            .eraseToAnyPublisher()
    }
}
