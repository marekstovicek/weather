//
//  MapView.swift
//  Weather
//
//  Created by Marek Štovíček on 24/10/2020.
//

import Foundation
import MapKit
import SwiftUI

struct MapView: View {
    
    // MARK: - Variables
    // bindable
    @Binding var coordinates: MKCoordinateRegion

    var body: some View {
        Map(coordinateRegion: $coordinates)
        .edgesIgnoringSafeArea(.all)
    }
}
