//
//  WeatherView.swift
//  Weather
//
//  Created by Marek Štovíček on 10/10/2020.
//

import SwiftUI
import Combine
import MapKit

struct WeatherView: View {

    // MARK: - Variables

    var content: Content

    // MARK: - Body
    var body: some View {
        VStack {
            Text(content.city)
                .font(.headline)
                .foregroundColor(Color.white)
                .padding(.top)
            HStack {
                content.imageURL.map { url in
                    URLImage(url: url)
                        .frame(width: 50, height: 50, alignment: .leading)
                        .padding()
                }
                Text("\(String(format: "%.1f °C", (content.temp)))")
                    .font(.title)
                    .foregroundColor(Color.white)
                    .padding()
            }
            
            VStack(alignment: .leading, spacing: 5) {
                Text("\(String(format: "Minimálně: %.1f °C", (content.min)))")
                    .foregroundColor(.white)
                Text("\(String(format: "Maximálně: %.1f °C", (content.max)))")
                    .foregroundColor(.white)
                    .padding(.bottom, 10)
            }
        }
        .background(Color.green)
        .cornerRadius(10)
    }
}

extension WeatherView {
    struct Content: Identifiable {
        let id = UUID()
        var temp: Double
        var min: Double
        var max: Double
        var city: String
        var imageURL: URL?
        var lat: Double
        var lon: Double
    }
}
