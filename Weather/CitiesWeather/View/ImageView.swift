//
//  ImageView.swift
//  Weather
//
//  Created by Marek Štovíček on 24/10/2020.
//

import Foundation
import SwiftUI
import Combine

struct URLImage: View {
    
    @ObservedObject var imageLoader: ImageLoader
    
    init(url: URL) {
        imageLoader = ImageLoader(url: url)
    }
    
    var body: some View {
        Group {
            Image(uiImage: imageLoader.image)
                .resizable()
        }
        .onAppear {
            imageLoader.load()
        }
        .onDisappear {
            imageLoader.cancel()
        }
    }
}

class ImageLoader: ObservableObject {
    
    @Published var url: URL
    @Published var image: UIImage = UIImage()
    
    private var cancellable: AnyCancellable?
    
    init(url: URL) {
        self.url = url
    }
    
    func load() {
        cancellable = URLSession.shared.dataTaskPublisher(for: url)
            .map(\.data)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { _ in
                //
            }, receiveValue: { data in
                self.image = UIImage(data: data) ?? UIImage()
            })
    }
    
    func cancel() {
        cancellable?.cancel()
    }
}
