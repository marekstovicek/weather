//
//  CitiesWeatherView.swift
//  Weather
//
//  Created by Marek Štovíček on 17/10/2020.
//

import SwiftUI
import MapKit

struct CitiesWeatherView: View {
    
    // MARK: - Variables
    // observable
    @ObservedObject var vm: WeatherVM
    
    // state
    @State var coordinates = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 0, longitude: 0),
                                                span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
    
    
    // MARK: - Initializer
    init(vm: WeatherVM) {
        self.vm = vm
    }
    
    var body: some View {
        NavigationView {
            VStack {
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack {
                        ForEach(vm.content) { content in
                            WeatherView(content: content)
                                .padding()
                                .onTapGesture {
                                    self.coordinates = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: content.lat, longitude: content.lon),
                                                                          span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
                                }
                        }
                    }
                }
                MapView(coordinates: $coordinates)
            }
            .navigationTitle("Počasí")
        }
    }
}
