//
//  WeatherVM.swift
//  Weather
//
//  Created by Marek Štovíček on 15/10/2020.
//

import Foundation
import Combine

class WeatherVM: ObservableObject {
        
    // MARK: - Variables
    // published
    @Published var content: [WeatherView.Content] = []
    
    // private
    private var cancelable = Set<AnyCancellable>()
    private let locationManager: LocationManager
    private let dataProvider: WeatherDataProviderType
    private var timer: Timer?
    private var imageURL: (String?) -> URL? = { urlString in
        guard let string = urlString, let url = URL(string: "https://openweathermap.org/img/wn/\(string)@2x.png") else {
            return nil
        }
        return url
    }
    
    // MARK: - Initializer
    init(dataProvider: WeatherDataProviderType, locationManager: LocationManager) {
        self.locationManager = locationManager
        self.dataProvider = dataProvider
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.loadData()
            self.startCurrentLocationFetching()
        }
    }
    
    deinit {
        timer?.invalidate()
    }
    
    // MARK: - Public
    
    func loadData() {
        let cityName = dataProvider.loadWeather(for: "New York")
        let current = dataProvider.loadLocationWeather(latitude: locationManager.coordinates.latitude,
                                                       longitude: locationManager.coordinates.longitude)
        Publishers.Zip(current, cityName).sink { (completion) in
            switch completion {
            case .failure(let error):
                print("Error data fetching: \(error.localizableString)")
            default:
                break
            }
        } receiveValue: { (current, cityName) in
            self.content = [
                WeatherView.Content(temp: current.main.temp - 273.15,
                                    min: current.main.temp_min - 273.15,
                                    max: current.main.temp_max - 273.15,
                                    city: current.name,
                                    imageURL: self.imageURL(current.weather.first?.icon),
                                    lat: current.coord.lat,
                                    lon: current.coord.lon),
                WeatherView.Content(temp: cityName.main.temp - 273.15,
                                    min: cityName.main.temp_min - 273.15,
                                    max: cityName.main.temp_max - 273.15,
                                    city: cityName.name,
                                    imageURL: self.imageURL(cityName.weather.first?.icon),
                                    lat: cityName.coord.lat,
                                    lon: cityName.coord.lon)
            ]
        }.store(in: &cancelable)
    }
    
    // MARK: - Private
    
    private func startCurrentLocationFetching() {
        timer = Timer.scheduledTimer(timeInterval: 30,
                                     target: self,
                                     selector: #selector(updateCurrentLocationData),
                                     userInfo: nil, repeats: true)
    }
    
    @objc private func updateCurrentLocationData() {
        loadData()
    }
}
