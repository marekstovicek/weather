//
//  ApiClient.swift
//  Weather
//
//  Created by Marek Štovíček on 15/10/2020.
//

import Foundation
import Combine

struct Router {
    var path: String
    var parameters: [String: String]? = nil
    var headers: [String: String]? = nil
}

protocol ApiClientType {
    func dataTaskPublisher<T: Decodable>(for router: Router, decoder: JSONDecoder?) -> AnyPublisher<T, NetworkError>
    func dataTaskPublisher(for url: String) -> AnyPublisher<Data, NetworkError>
}

class ApiClient: ApiClientType {

    private var baseURL: URL {
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/") else {
           fatalError()
        }
        return url
    }
    private let apiToken = "e72a7ca9eea7964968cbfc5661d3a890"
    
    func dataTaskPublisher<T>(for router: Router, decoder: JSONDecoder? = nil) -> AnyPublisher<T, NetworkError> where T : Decodable {
        URLSession.shared.dataTaskPublisher(for: makeUrl(router))
            .tryMap { (data, response) in
                let statusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
                switch statusCode {
                case 401:
                    throw NetworkError.unauthorized
                case (200..<300):
                    return data
                default:
                    throw NetworkError.internalError(statusCode)
                }
            }
            .decode(type: T.self, decoder: decoder ?? JSONDecoder())
            .mapError { error in
                switch error {
                case is Swift.DecodingError:
                    return NetworkError.deserializationError
                default:
                    return NetworkError.unknown(error)
                }
            }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    func dataTaskPublisher(for urlString: String) -> AnyPublisher<Data, NetworkError> {
        guard let url = URL(string: urlString) else {
            return Fail(error: NetworkError.invalidURL(urlString))
                .eraseToAnyPublisher()
        }
        return URLSession.shared.dataTaskPublisher(for: url)
            .tryMap { (data, response) in
                let statusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
                switch statusCode {
                case 401:
                    throw NetworkError.unauthorized
                case (200..<300):
                    return data
                default:
                    throw NetworkError.internalError(statusCode)
                }
            }
            .mapError { NetworkError.map($0) }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    private func makeUrl(_ router: Router) -> URL {
        let urlWithPath = baseURL.appendingPathComponent(router.path)
        var urlComponents = URLComponents(string: urlWithPath.absoluteString)
        let items = router.parameters?
                        .compactMap { $0 }
                        .map {
                            URLQueryItem(name: $0.key, value: $0.value)
                        }
        urlComponents?.queryItems = items
        urlComponents?.queryItems?.append(URLQueryItem(name: "appid", value: self.apiToken))
        guard let url = urlComponents?.url else {
            fatalError()
        }
        return url
    }
}
