//
//  NetworkError.swift
//  Weather
//
//  Created by Marek Štovíček on 24/10/2020.
//

import Foundation

enum NetworkError: Error {
    case deserializationError
    case unauthorized
    case badRequest
    case invalidURL(String)
    case invalidData
    case serverError
    case internalError(Int)
    case unknown(Error)
    
    var localizableString: String {
        switch self {
        case .deserializationError:
            return "Deserialization error"
        case .unauthorized:
            return "Unauthorized request"
        case .badRequest:
            return "Bad request"
        case .invalidURL(let urlString):
            return "Invalid url \(urlString)"
        case .invalidData:
            return "Cannot deserialize data"
        case .serverError:
            return "Server error"
        case .internalError(let statusCode):
            return "Internal error - status code \(statusCode)"
        case .unknown(let error):
            return "Unknown error: \(error.localizedDescription)"
        }
    }
    
    static func map(_ error: Error) -> NetworkError {
        return (error as? NetworkError) ?? .unknown(error)
    }
}
