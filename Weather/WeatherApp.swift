//
//  WeatherApp.swift
//  Weather
//
//  Created by Marek Štovíček on 10/10/2020.
//

import SwiftUI

@main
struct WeatherApp: App {
    var body: some Scene {
        WindowGroup {
            CitiesWeatherView(vm: WeatherVM(dataProvider: WeatherDataProvider(apiClient: ApiClient()),
                                            locationManager: LocationManager()))
        }
    }
}
